package com.prime.media.core

enum class FontFamily {

    // The default typography of app
    SYSTEM_DEFAULT,

    // The extra font provided with this app
    PROVIDED,

    // The sanserif font family
    SAN_SERIF,

    //
    SARIF,

    // The Dancing script font family.
    CURSIVE

}