# Audiofy: : Mp3, Mp4 Audio Player

An Open-Source Audio Player App for all your needs!

<a href="https://play.google.com/store/apps/details?id=com.prime.media" target="_blank">
<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" alt="Get it on Google Play" height="80"/></a></div>

## 🌞 Preview 

|   Shot-1    | Shot-2 | Shot-3 | Shot-4 | Shot-5
|---	        |---     |---     |---     |---
|  ![](https://user-images.githubusercontent.com/46754437/214095103-22ba813a-d1ea-4af6-b81c-651b3a411a47.png)    |  ![](https://user-images.githubusercontent.com/46754437/214095652-883aac27-6e39-456e-b532-91d891baddf5.png)  | ![](https://user-images.githubusercontent.com/46754437/214095925-b44b940f-c9ae-4c75-a255-a30c97059049.png)  | ![](https://user-images.githubusercontent.com/46754437/214096188-6c921f46-e5d9-44d6-a5f6-af9965ccecad.png) | ![](https://user-images.githubusercontent.com/46754437/214096540-ec4d0d87-4ea8-4a7b-b852-5431570d9299.png)
<br />

## Features
* Dark Theme.
* Single activity, compose archetecture
* Play any local audio track, progress bar
* Play Controller is able to be opened everywhere
* Large artist image loading, and can be used as background
* Connect with any 3rd party equilizer.
* Fast.
* etc

## 🎨 UI Design

***Click to View `Audiofy` app Design from below 👇***

[![Audiofy Figma Design](https://img.shields.io/badge/Adiofy-FIGMA-black.svg?style=for-the-badge&logo=figma)](https://www.figma.com/proto/HSXLNy0dNDHbBG9IkWfzTJ/Audiofy?node-id=11%3A40)

## 🛠 Built With

- [Kotlin](https://kotlinlang.org/) - First class and official programming language for Android
  development.
- [Jetpack Compose](https://developer.android.com/jetpack/compose) - Jetpack Compose is Android’s
  modern toolkit for building native UI.
- [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) - A coroutine is a
  concurrency design pattern that you can use on Android to simplify code that executes
  asynchronously.
- [Flow](https://kotlinlang.org/docs/reference/coroutines/flow.html) - A flow is an asynchronous
  version of a Sequence, a type of collection whose values are lazily produced.
- [Jetpack DataStore](https://developer.android.com/topic/libraries/architecture/datastore) -
  Jetpack DataStore is a data storage solution that allows you to store key-value pairs or typed
  objects with protocol buffers. DataStore uses Kotlin coroutines and Flow to store data
  asynchronously, consistently, and transactionally.
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture) -
  Collection of libraries that help you design robust, testable, and maintainable apps.
    - [Stateflow](https://developer.android.com/kotlin/flow/stateflow-and-sharedflow) - StateFlow is
      a state-holder observable flow that emits the current and new state updates to its collectors.
    - [Flow](https://kotlinlang.org/docs/reference/coroutines/flow.html) - A flow is an asynchronous
      version of a Sequence, a type of collection whose values are lazily produced.
    - [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Stores
      UI-related data that isn"t destroyed on UI changes.
    - [Jetpack Compose Navigation](https://developer.android.com/jetpack/compose/navigation) - The
      Navigation component provides support for Jetpack Compose applications.
    - [DataStore](https://developer.android.com/topic/libraries/architecture/datastore) - Jetpack
      DataStore is a data storage solution that allows you to store key-value pairs or typed objects
      with protocol buffers. DataStore uses Kotlin coroutines and Flow to store data asynchronously,
      consistently, and transactionally.
- [Material Components for Android](https://github.com/material-components/material-components-android)
    - Modular and customizable Material Design UI components for Android.
- [Accompanist](https://github.com/google/accompanist)
    - A collection of extension libraries for Jetpack Compose.
- [Figma](https://figma.com/) - Figma is a vector graphics editor and prototyping tool which is
  primarily web-based.

<br />

## 🗼 Architecture

This app uses [***MVVM (Model View
View-Model)***](https://developer.android.com/jetpack/docs/guide#recommended-app-arch) architecture.

![](https://github.com/TheCodeMonks/Notes-App/blob/master/screenshots/ANDROID%20ROOM%20DB%20DIAGRAM.jpg)

## 🧰 Build-tool

You need to
have [Android Studio (ElectricEeel) or above](https://developer.android.com/studio) to
build this project.
<br>

<img src="./beta_android.png" height="200" alt="Beta-studio"/>

## ❤️ Show your Support

I love it when people use my tool and we’d love to make it even better. If you like this tool and
want to support me in developing more free tools for you, I’d really appreciate a donation. Feel
free to `buy me a cup of coffee` 😄. Thanks!


<a href="https://www.buymeacoffee.com/sheikhzaki3" target="_blank">
    <img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" width="160">
</a>

<br>


## 📩 Contact

I know that first and foremost you are looking for a tool to solve your problems, but if you enjoy
it that much, why not tell us? We would love to hear from you 😉

DM me at 👇

* Twitter: <a href="https://twitter.com/zakirsheikh74" target="_blank">@zakirsheikh74</a>
* Email: feedbacktoprime@gmail.com

<br>

## 🔖 License

```
    Apache 2.0 License


    Copyright 2021 Spikey sanju

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

```
